﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="Default.aspx.cs" Inherits="MenuWeb.Default" %>

<!DOCTYPE html>  
<html xmlns="http://www.w3.org/1999/xhtml">  
<head runat="server">  
    <title></title>  
    <script src="js/jquery-1.7.1.js" type="text/javascript"></script>
     <script>  
         $(document).ready(function () {  
             
             $("#Save").click(function () {  
                 var person = new Object();  
                 person.name = "Dum - ";  
                 person.surname = "Soken";  
                 $.ajax({  
                     url: 'http://localhost:53053/api/GetUserPermission',  
                     type: 'POST',  
                     dataType: 'json',  
                     data: person,  
                     success: function (data, textStatus, xhr) {  
                         console.log(data);  
                     },  
                     error: function (xhr, textStatus, errorThrown) {  
                         console.log('Error in Operation');  
                     }  
                 });  
             });  
         });  
     </script>  
</head>  
<body>  
    <form id="form1">  
        Name :- <input type="text" name="name" id="name" />  
        Surname:- <input type="text" name="surname" id="surname" />  
        <input type="button" id="Save" value="Save Data" />  
    </form>  
</body>  
</html>  
